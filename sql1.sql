use data;
select * from student;
create table employee(empid int primary key,
						ename varchar(20),
                        job_desc varchar(30),
                        salary int ,
                        Hire_date date);
show tables;
select * from employee;
alter table employee add location varchar(20);
alter table employee drop location;
insert into  employee values(1,"Ram","Admin",1000000,'2022-05-13');
select * from employee;
INSERT INTO employee VALUES(2,'Harini','MANAGER','2500000','2023-08-12');
INSERT INTO employee VALUES(3,'George','SALES','2000000','2022-07-12');
INSERT INTO employee VALUES(4,'Ramya','SALES','1300000','2023-02-11');
INSERT INTO employee VALUES(5,'Meena','HR','2000000','2021-04-12');
INSERT INTO employee VALUES(6,'Ashok','MANAGER','3000000','2020-03-22');
INSERT INTO employee VALUES(7,'Abdul','HR','2000000','2020-08-14');
INSERT INTO employee VALUES(8,'Ramya','ENGINEER','1000000','2022-02-12');
INSERT INTO employee VALUES(9,'Raghu','CEO','8000000','2016-04-20');
INSERT INTO employee VALUES(10,'Arvind','MANAGER','2800000','2017-08-22');
INSERT INTO employee VALUES(11,'Akshay','ENGINEER','1000000','2021-05-21');
INSERT INTO employee VALUES(12,'John','ADMIN','2200000','2021-04-18');
INSERT INTO employee VALUES(13,'Abinaya','ENGINEER','2100000','2022-08-22');
select * from employee;
update employee set job_desc="Analyst" where job_desc="Engineer";
update employee set job_desc="ag" where job_desc="hr";
select * from employee;
select job_desc,max(salary),count(job_desc) count from employee group by job_desc ;
select job_desc,count(job_desc) count from employee where salary>1500000 group by job_desc having count>1 order by job_desc;
alter table employee drop hire_date;
select * from employee;
alter table employee modify empid int  auto_increment;
alter table employee modify ename varchar(20) not null;
select * from employee;
create table branch(branch_id int primary key auto_increment,
					branch_name varchar(20) not null,
					address varchar(30));

select * from branch;
insert into  branch values(1,"chennai",'16 ABC Road'),(2,"Coimbatore",'21 Red-Block street'),(3,'Mumbai','25 XYZ Road');
select * from employee;
alter table employee add branch_id int ;
alter table employee add  foreign key (branch_id) references branch(branch_id);
alter table employee drop branch_id;
select * from employee;
select * from branch;
alter table branch add Manager_id int;
alter table branch drop Manager_id;
select * from branch;
alter table branch add foreign key (Manager_id) references employee(empid);
drop table branch;
show tables;
select * from employee;
drop table employee;
create table branch1(
branch_id INT PRIMARY KEY AUTO_INCREMENT,
br_name VARCHAR(30) NOT NULL,
addr VARCHAR(200) );

CREATE TABLE employee1 (
emp_id INT PRIMARY KEY,
ename VARCHAR(30),
job_desc VARCHAR(20),
salary INT,
branch_id INT,
CONSTRAINT FK_branchId FOREIGN KEY(branch_id) REFERENCES branch1(branch_id));
select * from employee1;
insert into  branch1 values(1,"chennai",'16 ABC Road'),(2,"Coimbatore",'21 Red-Block street'),(3,'Mumbai','25 XYZ Road');
select * from branch1;
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(1,'Ram','ADMIN',1000000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(2,'Harini','MANAGER',2500000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(3,'George','SALES',2000000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(4,'Ramya','SALES',1300000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(5,'Meena','HR',2000000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(6,'Ashok','MANAGER',3000000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(7,'Abdul','HR',2000000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(8,'Ramya','ENGINEER',1000000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(9,'Raghu','CEO',8000000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(10,'Arvind','MANAGER',2800000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(11,'Akshay','ENGINEER',1000000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(12,'John','ADMIN',2200000);
INSERT INTO employee1(emp_id,ename,job_desc,salary) VALUES(13,'Abinaya','ENGINEER',2100000);
select * from employee1;
select * from branch1;
-- drop table branch1;
-- show tables;
-- drop table employee1;
alter table branch1 add Manager_id int;
alter table branch1 add constraint mn_fk foreign key(Manager_id) references employee1(empid);
select * from employee1;
select * from branch1;
SHOW INDEX FROM employee;
show tables;
drop table branch1;
drop table employee1;

CREATE TABLE branch2(
branch_id INT PRIMARY KEY AUTO_INCREMENT,
br_name VARCHAR(30) NOT NULL,
addr VARCHAR(200) );

CREATE TABLE employee2 (
emp_id INT PRIMARY KEY AUTO_INCREMENT,
ename VARCHAR(30) NOT NULL,
job_desc VARCHAR(20),
salary INT,
branch_id INT,
CONSTRAINT FK_branchId1 FOREIGN KEY(branch_id) REFERENCES branch2(branch_id) 
ON DELETE CASCADE 
);
INSERT INTO branch2 VALUES(1,"Chennai","16 ABC Road");
INSERT INTO branch2 VALUES(2,"Coimbatore","120 15th Block");
INSERT INTO branch2 VALUES(3,"Mumbai","25 XYZ Road");
INSERT INTO branch2 VALUES(4,"Hydrabad","32 10th Street");

select * from branch2;
INSERT INTO employee2 VALUES(1,'Ram','ADMIN',1000000,2);
INSERT INTO employee2 VALUES(2,'Harini','MANAGER',2500000,2);
INSERT INTO employee2 VALUES(3,'George','SALES',2000000,1);
INSERT INTO employee2 VALUES(4,'Ramya','SALES',1300000,2);
INSERT INTO employee2 VALUES(5,'Meena','HR',2000000,3);
INSERT INTO employee2 VALUES(6,'Ashok','MANAGER',3000000,1);
INSERT INTO employee2 VALUES(7,'Abdul','HR',2000000,1);
INSERT INTO employee2 VALUES(8,'Ramya','ENGINEER',1000000,2);
INSERT INTO employee2 VALUES(9,'Raghu','CEO',8000000,3);
INSERT INTO employee2 VALUES(10,'Arvind','MANAGER',2800000,3);
INSERT INTO employee2 VALUES(11,'Akshay','ENGINEER',1000000,1);
INSERT INTO employee2 VALUES(12,'John','ADMIN',2200000,1);
INSERT INTO employee2 VALUES(13,'Abinaya','ENGINEER',2100000,2);
INSERT INTO employee2 VALUES(14,'Vidya','ADMIN',2200000,NULL);
INSERT INTO employee2 VALUES(15,'Ranjani','ENGINEER',2100000,NULL);

select * from employee2;
delete from branch2 where branch_id="2";
select * from branch2;

CREATE TABLE branch3(
branch_id INT PRIMARY KEY AUTO_INCREMENT,
br_name VARCHAR(30) NOT NULL,
addr VARCHAR(200) );

CREATE TABLE employee3 (
emp_id INT PRIMARY KEY AUTO_INCREMENT,
ename VARCHAR(30) NOT NULL,
job_desc VARCHAR(20),
salary INT,
branch_id INT,
CONSTRAINT FK_branchId2 FOREIGN KEY(branch_id) REFERENCES branch3(branch_id) 
);
drop table employee3;
INSERT INTO branch3 VALUES(1,"Chennai","16 ABC Road");
INSERT INTO branch3 VALUES(2,"Coimbatore","120 15th Block");
INSERT INTO branch3 VALUES(3,"Mumbai","25 XYZ Road");
INSERT INTO branch3 VALUES(4,"Hydrabad","32 10th Street");

select * from branch3;

INSERT INTO employee3 VALUES(1,'Ram','ADMIN',1000000,2);
INSERT INTO employee3 VALUES(2,'Harini','MANAGER',2500000,2);
INSERT INTO employee3 VALUES(3,'George','SALES',2000000,1);
INSERT INTO employee3 VALUES(4,'Ramya','SALES',1300000,2);
INSERT INTO employee3 VALUES(5,'Meena','HR',2000000,3);
INSERT INTO employee3 VALUES(6,'Ashok','MANAGER',3000000,1);
INSERT INTO employee3 VALUES(7,'Abdul','HR',2000000,1);
INSERT INTO employee3 VALUES(8,'Ramya','ENGINEER',1000000,2);
INSERT INTO employee3 VALUES(9,'Raghu','CEO',8000000,3);
INSERT INTO employee3 VALUES(10,'Arvind','MANAGER',2800000,3);
INSERT INTO employee3 VALUES(11,'Akshay','ENGINEER',1000000,1);
INSERT INTO employee3 VALUES(12,'John','ADMIN',2200000,1);
INSERT INTO employee3 VALUES(13,'Abinaya','ENGINEER',2100000,2);
INSERT INTO employee3 VALUES(14,'Vidya','ADMIN',2200000,NULL);
INSERT INTO employee3 VALUES(15,'Ranjani','ENGINEER',2100000,NULL);
select * from employee3;
select * from branch3;
select e.emp_id,e.ename,e.job_desc,b.br_name
from employee3 as e
inner join branch3 as b
on e.branch_id=b.branch_id
order by e.emp_id; 

-- br.name ,emp.count

select b.br_name,count(e.emp_id) as no_of_emp from employee3 as e 
join branch3 as b
on e.branch_id=b.branch_id
group by e.branch_id;
use data;


 
 

